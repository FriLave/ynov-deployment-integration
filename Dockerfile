FROM php:apache
# Install pdo_mysql extension
RUN docker-php-ext-install pdo_mysql
COPY src/ /var/www/html/
EXPOSE 80
CMD ["apachectl", "-D", "FOREGROUND"]

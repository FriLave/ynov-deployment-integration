CREATE TABLE IF NOT EXISTS city
(
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (name)
);


INSERT INTO city (name) VALUES
                            ('New York'),
                            ('Paris'),
                            ('Bordeaux')
ON DUPLICATE KEY UPDATE name = name;

